package com.semanasanta.caceres.commons.model

class Paso(var name: String?, val comments: String, val order: String, val brothers: String, val type: String, val uri: String, val imageUri: String, val imageAuthor: String, val processionUri: String)
