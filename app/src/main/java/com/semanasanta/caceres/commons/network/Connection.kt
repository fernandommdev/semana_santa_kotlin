package com.semanasanta.caceres.commons.network

import java.io.IOException

import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

class Connection {

    private var operationsFinishedListener: OperationsFinishedListener? = null

    private var routeOperationFinishedListener: RouteOperationFinishedListener? = null

    fun setOperationsFinishedListener(operationsFinishedListener: OperationsFinishedListener) {
        this.operationsFinishedListener = operationsFinishedListener
    }

    fun setRouteOperationFinishedListener(routeOperationFinishedListener: RouteOperationFinishedListener) {
        this.routeOperationFinishedListener = routeOperationFinishedListener
    }

    fun requestProcessions() {
        val request = Request.Builder()
                .url("http://opendata.caceres.es/sparql/?default-graph-uri=&query=SELECT+%3Furi+%3Frdfs_label+%3Frdfs_comment+%3Fhabito+%28group_concat%28distinct+%3Fom_salid%3Bseparator%3D\"+-+\"%29+as+%3Fom_salida%29+%28group_concat%28distinct+%3Fom_recogid%3Bseparator%3D\"+-+\"%29+as+%3Fom_recogida%29+%28group_concat%28distinct+%3Fom_acompaniamientoMusical%3Bseparator%3D\"+-+\"%29+as+%3FacompaniamientoMusical%29+%28group_concat%28distinct+%3Ftime_at%3Bseparator%3D\"%23\"%29+as+%3Ftime_at%29+%3Furi_cofradia+%28group_concat%28distinct+%3Fom_pasos%3Bseparator%3D\"%23\"%29+as+%3Furis_pasos%29+%0D%0AWHERE+%7B+%0D%0A%3Furi+a+om%3AProcesion.+%0D%0AOPTIONAL++%7B%3Furi+rdfs%3Alabel+%3Frdfs_label.+%7D%0D%0AOPTIONAL++%7B%3Furi+rdfs%3Acomment+%3Frdfs_comment.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3Ahabito+%3Fhabito.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AacompaniamientoMusical+%3Fom_acompaniamientoMusical.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AtieneRuta+%3Ftiene_ruta.+%3Ftiene_ruta+time%3Aat+%3Ftime_at.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AtieneRuta+%3Ftiene_ruta.+%3Ftiene_ruta+om%3AsitioDeSalida+%3Fom_salid.%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AtieneRuta+%3Ftiene_ruta.+%3Ftiene_ruta+om%3AsitioDeRecogida+%3Fom_recogid.%7D%0D%0AOPTIONAL++%7B%3Furi_cofradia+om%3Aprocesiona+%3Furi.%7D%0D%0AOPTIONAL++%7B%3Fom_pasos+om%3AesLlevado+%3Furi.%7D%0D%0AFILTER+%28%3Ftime_at+>+\"2017-04-24T18%3A00%3A00\"%5E%5Exsd%3AdateTime%29%7D%0D%0AORDER+BY%28%3Ftime_at%29%0D%0A%0D%0A%0D%0A%0D%0A&format=json")
                .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                operationsFinishedListener?.onOperationFailed()
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                if (!response.isSuccessful) {
                    throw IOException("Unexpected code $response")
                }

                operationsFinishedListener?.onProcessionsOperationSuccess(response)
            }
        })
    }

    fun requestPasos() {
        val request = Request.Builder()
                .url("http://opendata.caceres.es/sparql/?default-graph-uri=&query=select+%3Furi+%3Frdfs_label+%3Frdfs_comment+%3Fom_esLlevado+%3Fom_numeroHermanosCarga+%3Fom_tipoCarga+%3Fom_ordenPaso+%3Fschema_image+%3Fschema_author+%28group_concat%28distinct+%3Fimage%3Bseparator%3D\"%3B+\"%29+as+%3Fimages%29+where%7B%0D%0A%3Furi+a+om%3APasoProcesional.%0D%0A%3Furi+rdfs%3Alabel+%3Frdfs_label.%0D%0A%3Furi+rdfs%3Acomment+%3Frdfs_comment.%0D%0A%3Furi+om%3AesLlevado+%3Fom_esLlevado.%0D%0A%3Furi+om%3AnumeroHermanosCarga+%3Fom_numeroHermanosCarga.%0D%0A%3Furi+om%3AtipoCarga+%3Fom_tipoCarga.%0D%0A%3Furi+om%3AordenPaso+%3Fom_ordenPaso.%0D%0A%3Furi+schema%3Aphoto+%3Fphoto.%0D%0A%3Fphoto+schema%3Aimage+%3Fschema_image.%0D%0A%3Fphoto+schema%3Aauthor+%3Fschema_author.%0D%0AOPTIONAL+%7B%3FuriImagen+om%3AformaParte+%3Furi.%0D%0A%3FuriImagen+rdfs%3Alabel+%3Fimage.%7D%0D%0A%7D%0D%0A&format=json")
                .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                operationsFinishedListener?.onOperationFailed()
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                if (!response.isSuccessful) {
                    throw IOException("Unexpected code $response")
                }

                operationsFinishedListener?.onPasosOperationSuccess(response)
            }
        })
    }

    fun requestRoute(processionUri: String) {
        val request = Request.Builder()
                .url("http://opendata.caceres.es/sparql/?default-graph-uri=&query=select+%28group_concat%28distinct+%3Fom_pasoAsociado%3Bseparator%3D\"%23\"%29+as+%3FpasosAsociados%29+%3Ftiene_Punto+%3Flat+%3Flong+%3Forden_Punto+where%7B%0D%0A<$processionUri>+a+om%3AProcesion.%0D%0A<$processionUri>+om%3AtieneRuta+%3Fom_tieneRuta.%0D%0A%3Fom_tieneRuta+om%3ApasoAsociado+%3Fom_pasoAsociado.%0D%0A%3Fom_tieneRuta+om%3AtienePunto+%3Ftiene_Punto.%0D%0A%3Ftiene_Punto+om%3AordenPunto+%3Forden_Punto.%0D%0A%3Ftiene_Punto+geo%3Alat+%3Flat.%0D%0A%3Ftiene_Punto+geo%3Along+%3Flong.+%0D%0AFILTER+regex%28str%28%3Ftiene_Punto%29%2C+\"2018\"%29+.%0D%0A%7D+ORDER+BY%28%3Forden_Punto%29%0D%0A%0D%0A%0D%0A%0D%0A&format=json")
                .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                routeOperationFinishedListener?.onOperationFailed()
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                if (!response.isSuccessful) {
                    throw IOException("Unexpected code $response")
                }

                routeOperationFinishedListener?.onOperationRouteSuccess(response)
            }
        })
    }

    fun requestBrotherhoods() {
        val request = Request.Builder()
                .url("http://opendata.caceres.es/sparql?default-graph-uri=&query=SELECT+%3Furi+%3Fschema_url+%3Fom_nombreAsociacion+%3Fom_numeroMiembros+%3Fom_nombrePopularAsociacion+%28group_concat%28distinct+%3Fom_procesiona%3Bseparator%3D\"%23\"%29+as+%3Fprocesiona%29+%3Fom_anoFundacion+%3Fenlace_wikipedia+%0D%0A%28group_concat+%28distinct+%28concat%28%3Frss_name%2C\"%2F\"%2C+%3Faccount_name%29%29%3Bseparator%3D\"%23\"%29+as+%3Frss_links%29%0D%0AWHERE+%7B+%0D%0A%3Furi+a+om%3ACofradia.+%0D%0AOPTIONAL++%7B%3Furi+schema%3Aurl+%3Fschema_url.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AnombreAsociacion+%3Fom_nombreAsociacion.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AnumeroMiembros+%3Fom_numeroMiembros.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AnombrePopularAsociacion+%3Fom_nombrePopularAsociacion.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3Aprocesiona+%3Fom_procesiona.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AanoFundacion+%3Fom_anoFundacion.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AtieneEnlaceWikipedia+%3Fenlace_wikipedia.+%7D%0D%0AOPTIONAL++%7B%3Furi+foaf%3Aaccount+%3Faccount.+OPTIONAL%7B%3Faccount+foaf%3AaccountName+%3Faccount_name.%7D+OPTIONAL%7B%3Faccount+foaf%3AaccountServiceHomepage+%3Frss_name.%7D%7D%0D%0A%7D%0D%0A&format=json")
                .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                operationsFinishedListener?.onOperationFailed()
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                if (!response.isSuccessful) {
                    throw IOException("Unexpected code $response")
                }

                operationsFinishedListener?.onBrotherhoodsOperationSuccess(response)
            }
        })
    }

    interface OperationsFinishedListener {
        fun onProcessionsOperationSuccess(response: Response)
        fun onPasosOperationSuccess(response: Response)
        fun onBrotherhoodsOperationSuccess(response: Response)
        fun onOperationFailed()
    }

    interface RouteOperationFinishedListener {
        fun onOperationRouteSuccess(response: Response)
        fun onOperationFailed()
    }

    companion object {
        private val client = OkHttpClient()
        private var connection: Connection? = null

        val instance: Connection
            get() {
                if (connection == null) {
                    connection = Connection()
                }
                return connection as Connection
            }
    }
}
