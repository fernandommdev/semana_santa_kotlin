package com.semanasanta.caceres.commons.tools

import android.content.Context
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

object ImageLoader {
    fun loadSimpleImageOnImageView(context: Context, imageUri: String, imageView: ImageView?) {
        imageView?.let {
            Picasso.with(context).load(imageUri).into(it)
        }
    }
    fun loadImageOnImageView(context: Context, imageUri: String, imageView: ImageView?, callback: Callback?) {
        imageView?.let {
            Picasso.with(context).load(imageUri).fit().centerCrop().into(it, callback)
        }
    }
}