package com.semanasanta.caceres.commons.model

class Festivity(var name: String?, var date: String?, private var processionsArray: MutableList<Procession>) {

    fun getProcessionsArray(): List<Procession> {
        return processionsArray
    }

    fun addProcession(procession: Procession) {
        this.processionsArray.add(procession)
    }
}
