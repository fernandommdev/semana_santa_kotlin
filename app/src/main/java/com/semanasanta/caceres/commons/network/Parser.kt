package com.semanasanta.caceres.commons.network

import android.app.Activity

import com.semanasanta.caceres.commons.model.Brotherhood
import com.semanasanta.caceres.commons.model.Paso
import com.semanasanta.caceres.commons.model.Point
import com.semanasanta.caceres.commons.model.Procession
import com.semanasanta.caceres.commons.model.RssEntry

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.util.ArrayList
import java.util.Arrays
import java.util.HashMap

import okhttp3.Response

object Parser {

    fun parseProcessionsJson2(response: Response, activity: Activity): ArrayList<Procession> {
        val result = ArrayList<Procession>()
        val jsonObject: JSONObject
        val jsonArray: JSONArray
        try {
            jsonObject = JSONObject(response.body()!!.string())
            jsonArray = jsonObject.getJSONObject("results").getJSONArray("bindings")
            for (i in 0 until jsonArray.length()) {
                val jsonobject = jsonArray.getJSONObject(i)
                val uri = jsonobject.getJSONObject("uri").getString("value")
                val name = jsonobject.getJSONObject("rdfs_label").getString("value")
                val comments = jsonobject.getJSONObject("rdfs_comment").getString("value")
                val clothes = jsonobject.getJSONObject("habito").getString("value")
                val musicians = jsonobject.getJSONObject("acompaniamientoMusical").getString("value")
                val date = jsonobject.getJSONObject("time_at").getString("value")
                val departure = jsonobject.getJSONObject("om_salida").getString("value")
                val arrival = jsonobject.getJSONObject("om_recogida").getString("value")
                val pasos = jsonobject.getJSONObject("uris_pasos").getString("value")
                val listPasos = ArrayList(Arrays.asList(*pasos.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()))
                val procession = Procession(uri, name, comments, date, departure, arrival, listPasos, clothes, musicians)
                result.add(procession)
            }
        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return result
    }

    fun parsePasoJson2(response: Response, activity: Activity): HashMap<String, Paso> {
        val result = HashMap<String, Paso>()
        val jsonObject: JSONObject
        val jsonArray: JSONArray
        try {
            jsonObject = JSONObject(response.body()!!.string())
            jsonArray = jsonObject.getJSONObject("results").getJSONArray("bindings")
            for (i in 0 until jsonArray.length()) {
                val jsonobject = jsonArray.getJSONObject(i)
                val uri = jsonobject.getJSONObject("uri").getString("value")
                val name = jsonobject.getJSONObject("rdfs_label").getString("value")
                val comments = jsonobject.getJSONObject("rdfs_comment").getString("value")
                val brothers = jsonobject.getJSONObject("om_numeroHermanosCarga").getString("value")
                val type = jsonobject.getJSONObject("om_tipoCarga").getString("value")
                val order = jsonobject.getJSONObject("om_ordenPaso").getString("value")
                val imageUri = jsonobject.getJSONObject("schema_image").getString("value")
                val imageAuthor = jsonobject.getJSONObject("schema_author").getString("value")
                val processionUri = jsonobject.getJSONObject("om_esLlevado").getString("value")
                val paso = Paso(name, comments, order, brothers, type, uri, imageUri, imageAuthor, processionUri)
                result[paso.uri] = paso
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return result
    }

    fun parseBrotherhoodsJson(response: Response, activity: Activity): ArrayList<Brotherhood> {
        val result = ArrayList<Brotherhood>()
        val jsonObject: JSONObject
        val jsonArray: JSONArray
        try {
            jsonObject = JSONObject(response.body()!!.string())
            jsonArray = jsonObject.getJSONObject("results").getJSONArray("bindings")
            for (i in 0 until jsonArray.length()) {
                val jsonobject = jsonArray.getJSONObject(i)
                val uri = jsonobject.getJSONObject("uri").getString("value")
                val name = jsonobject.getJSONObject("om_nombreAsociacion").getString("value")
                val popularName = jsonobject.getJSONObject("om_nombrePopularAsociacion").getString("value")
                var web = ""
                try {
                    web = jsonobject.getJSONObject("schema_url").getString("value")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                val founded = jsonobject.getJSONObject("om_anoFundacion").getString("value")
                val members = jsonobject.getJSONObject("om_numeroMiembros").getString("value")

                val rssEntries = HashMap<RssEntry.SocialNetwork, RssEntry>()
                var enlaceWikipedia = ""
                try {
                    enlaceWikipedia = jsonobject.getJSONObject("enlace_wikipedia").getString("value")
                    val rssEntry = RssEntry(enlaceWikipedia)
                    rssEntries[rssEntry.socialNetwork] = rssEntry
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                try {
                    val rssLinks = jsonobject.getJSONObject("rss_links").getString("value")
                    if (rssLinks != null && !rssLinks.isEmpty()) {
                        val aux = ArrayList(Arrays.asList(*rssLinks.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()))
                        for (j in aux.indices) {
                            val rssEntry = RssEntry(aux[j])
                            rssEntries[rssEntry.socialNetwork] = rssEntry
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }


                val uriProcession = jsonobject.getJSONObject("procesiona").getString("value")
                val listProcessions = ArrayList(Arrays.asList(*uriProcession.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()))
                val brotherhood = Brotherhood(uri, name, popularName, web, members, founded, listProcessions, rssEntries)
                result.add(brotherhood)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return result
    }

    fun parseRouteJson(response: Response): HashMap<String, ArrayList<Point>> {
        val resultAux = ArrayList<Point>()
        val result = HashMap<String, ArrayList<Point>>()
        val jsonObject: JSONObject
        val jsonArray: JSONArray
        try {
            jsonObject = JSONObject(response.body()!!.string())
            jsonArray = jsonObject.getJSONObject("results").getJSONArray("bindings")
            for (i in 0 until jsonArray.length()) {
                val jsonobject = jsonArray.getJSONObject(i)
                val uriPasos = jsonobject.getJSONObject("pasosAsociados").getString("value")
                val latitude = java.lang.Double.valueOf(jsonobject.getJSONObject("lat").getString("value"))
                val longitude = java.lang.Double.valueOf(jsonobject.getJSONObject("long").getString("value"))
                val order = Integer.valueOf(jsonobject.getJSONObject("orden_Punto").getString("value"))
                val point = Point(uriPasos, latitude, longitude, order)
                resultAux.add(point)
                var aux = ArrayList<Point>()
                if (result[uriPasos] != null) {
                    aux = ArrayList(result[uriPasos]!!)
                }
                aux.add(point)
                result[uriPasos] = aux
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return result
    }
}
