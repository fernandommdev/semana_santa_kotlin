package com.semanasanta.caceres.commons.model

import java.util.*

class Procession(val uri: String, val name: String, val comments: String, val date: String, val departure: String, val arrival: String, val pasos: ArrayList<String>, val clothes: String, val musicians: String)