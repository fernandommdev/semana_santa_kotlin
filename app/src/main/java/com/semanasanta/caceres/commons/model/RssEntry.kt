package com.semanasanta.caceres.commons.model

class RssEntry(val url: String) {
    val socialNetwork: SocialNetwork

    init {
        this.socialNetwork = parseSocialNetwork(url)
    }

    private fun parseSocialNetwork(url: String): SocialNetwork {
        return when {
            url.contains("facebook") -> SocialNetwork.FACEBOOK
            url.contains("twitter") -> SocialNetwork.TWITTER
            url.contains("pinterest") -> SocialNetwork.PINTEREST
            url.contains("instagram") -> SocialNetwork.INSTAGRAM
            url.contains("linkedin") -> SocialNetwork.LINKEDIN
            url.contains("wikipedia") -> SocialNetwork.WIKIPEDIA
            else -> SocialNetwork.UNKNOWN
        }
    }

    enum class SocialNetwork {
        FACEBOOK,
        TWITTER,
        PINTEREST,
        INSTAGRAM,
        LINKEDIN,
        WIKIPEDIA,
        UNKNOWN
    }
}
