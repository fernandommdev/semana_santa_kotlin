package com.semanasanta.caceres.commons.model

import java.util.*

class Brotherhood(val uri: String, val name: String, val popularName: String, val web: String, val members: String, val founded: String, val uriProcession: ArrayList<String>, val rssEntries: HashMap<RssEntry.SocialNetwork, RssEntry>)
