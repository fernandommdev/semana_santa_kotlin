package com.semanasanta.caceres

import android.app.Application
import com.semanasanta.caceres.commons.model.Brotherhood
import com.semanasanta.caceres.commons.model.Paso
import com.semanasanta.caceres.commons.model.Procession
import java.util.*

class MyApplication : Application() {
    var processions = ArrayList<Procession>()

    var pasos = HashMap<String, Paso>()

    var brotherhoods = ArrayList<Brotherhood>()
}
