package com.semanasanta.caceres.ui.activities

import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast

import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.semanasanta.caceres.MyApplication
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.network.Connection
import com.semanasanta.caceres.commons.network.Parser
import com.semanasanta.caceres.ui.fragments.BrotherhoodsListFragment
import com.semanasanta.caceres.ui.fragments.CustomDialogFragment
import com.semanasanta.caceres.ui.fragments.GalleryFragment
import com.semanasanta.caceres.ui.fragments.ProcessionsListFragment

import okhttp3.Response

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, Connection.OperationsFinishedListener {

    private var con: Connection? = null
    private var progressDialog: ProgressDialog? = null
    private var lastMenuOptionSelected = 7

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713")

        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.setDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)

        tryAgainButton.setOnClickListener { updateData() }
        updateData()
    }


    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    private fun checkLastMenuOptionClicked() {
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        val fragment: Fragment
        when (lastMenuOptionSelected) {
            0 -> {
                fragment = ProcessionsListFragment.newInstance()
                ft.replace(R.id.fragment_container, fragment, ProcessionsListFragment.TAG)
                ft.commit()
                navigationView.setCheckedItem(R.id.nav_processions)
            }
            1 -> {
                fragment = GalleryFragment.newInstance()
                ft.replace(R.id.fragment_container, fragment, GalleryFragment.TAG)
                ft.commit()
                navigationView.setCheckedItem(R.id.nav_gallery)
            }
            2 -> {
                fragment = BrotherhoodsListFragment.newInstance()
                ft.replace(R.id.fragment_container, fragment, BrotherhoodsListFragment.TAG)
                ft.commit()
                navigationView.setCheckedItem(R.id.nav_brotherhood)
            }
            3 -> {
            }
            else -> {
                fragment = ProcessionsListFragment.newInstance()
                ft.replace(R.id.fragment_container, fragment, ProcessionsListFragment.TAG)
                ft.commit()
                navigationView.setCheckedItem(R.id.nav_processions)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        val fragment: Fragment

        val id = item.itemId
        if (id == R.id.nav_processions) {
            if (lastMenuOptionSelected != 0) {
                fragment = ProcessionsListFragment.newInstance()
                ft.replace(R.id.fragment_container, fragment, ProcessionsListFragment.TAG)
                ft.addToBackStack(null)
                ft.commit()
                lastMenuOptionSelected = 0
            }
        } else if (id == R.id.nav_gallery) {
            if (lastMenuOptionSelected != 1) {
                fragment = GalleryFragment.newInstance()
                ft.replace(R.id.fragment_container, fragment, GalleryFragment.TAG)
                ft.addToBackStack(null)
                ft.commit()
                lastMenuOptionSelected = 1
            }
        } else if (id == R.id.nav_brotherhood) {
            if (lastMenuOptionSelected != 2) {
                fragment = BrotherhoodsListFragment.newInstance()
                ft.replace(R.id.fragment_container, fragment, BrotherhoodsListFragment.TAG)
                ft.addToBackStack(null)
                ft.commit()
                lastMenuOptionSelected = 2
            }
        } else if (id == R.id.nav_info) {
            val fragmentDialog = CustomDialogFragment.newInstance()
            fragmentDialog.show(fm, "fragmentDialog")
        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun updateData() {
        tryAgainButton.visibility = View.GONE
        val myApplication = application as MyApplication
        if (myApplication.processions.size == 0 || myApplication.pasos.size == 0) {
            progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(getString(R.string.data_update))
            progressDialog!!.setMessage(getString(R.string.wait_please))
            progressDialog!!.setCancelable(false)
            progressDialog!!.show()
            con = Connection.instance
            con!!.setOperationsFinishedListener(this)
            try {
                con!!.requestProcessions()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            checkLastMenuOptionClicked()
        }
    }

    override fun onProcessionsOperationSuccess(response: Response) {
        val result = Parser.parseProcessionsJson2(response, this)
        val myApplication = application as MyApplication
        myApplication.processions = result
        con = Connection.instance
        try {
            con!!.requestPasos()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onPasosOperationSuccess(response: Response) {
        val pasos = Parser.parsePasoJson2(response, this)
        val myApplication = application as MyApplication
        myApplication.pasos = pasos

        con = Connection.instance
        try {
            con!!.requestBrotherhoods()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onBrotherhoodsOperationSuccess(response: Response) {
        val brotherhoods = Parser.parseBrotherhoodsJson(response, this)
        val myApplication = application as MyApplication
        myApplication.brotherhoods = brotherhoods

        this.runOnUiThread { navigationView.setCheckedItem(R.id.nav_processions) }

        val fragment = ProcessionsListFragment.newInstance()
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        ft.replace(R.id.fragment_container, fragment, ProcessionsListFragment.TAG)
        ft.commitAllowingStateLoss()
        lastMenuOptionSelected = 0

        progressDialog!!.cancel()
    }

    override fun onOperationFailed() {
        this.runOnUiThread {
            progressDialog!!.cancel()
            tryAgainButton.visibility = View.VISIBLE
            Toast.makeText(applicationContext, getString(R.string.error_downloading_data), Toast.LENGTH_SHORT).show()
        }
    }
}
