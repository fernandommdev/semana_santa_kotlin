package com.semanasanta.caceres.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.model.Festivity
import com.semanasanta.caceres.commons.model.Paso
import com.semanasanta.caceres.commons.tools.ImageLoader
import com.squareup.picasso.Callback
import java.util.*

class ProcessionsAdapter(private val items: List<Festivity>, private val pasos: HashMap<String, Paso>, private val context: Context, private val op: OpenDialogListener) : SectionedRecyclerViewAdapter<ProcessionsAdapter.ViewHolder>() {

    override fun getSectionCount(): Int {
        return items.size
    }

    override fun getItemCount(section: Int): Int {
        return items[section].getProcessionsArray().size
    }

    override fun onBindHeaderViewHolder(holder: ViewHolder, section: Int) {
        holder.title?.text = items[section].name
        holder.dateTitle?.text = items[section].date
    }

    override fun onBindViewHolder(holder: ViewHolder, section: Int, relativePosition: Int, absolutePosition: Int) {
        holder.title?.text = items[section].getProcessionsArray()[relativePosition].name
        var pasoPositionSelected = 0
        val procession = items[section].getProcessionsArray()[relativePosition]
        if (procession.name.contains("Encuentro") || procession.name.contains("Cena") || procession.name.contains("Amor")) {
            if (procession.pasos.size > 1) {
                pasoPositionSelected = procession.pasos.size - 1
            }
        }

        val imageUri = pasos[items[section].getProcessionsArray()[relativePosition].pasos[pasoPositionSelected]]?.imageUri
        imageUri?.let {
            ImageLoader.loadImageOnImageView(context, it, holder.processionImage, object : Callback {
                override fun onSuccess() {
                    holder.loadingTextView?.visibility = View.GONE
                }

                override fun onError() {
                    holder.loadingTextView?.text = context.getString(R.string.error_loading_images)
                }
            })
        }

        val processionId = items[section].getProcessionsArray()[relativePosition].uri
        holder.processionImage?.setOnClickListener {
            op.onNavigateToDetailsScreen(processionId)
        }
        holder.detailsButton?.setOnClickListener {
            op.onNavigateToDetailsScreen(processionId)
        }

        holder.mapButton?.setOnClickListener {
            op.onNavigateToMapScreen(processionId)
        }

        // Setup non-header view.
        // 'section' is section index.
        // 'relativePosition' is index in this section.
        // 'absolutePosition' is index out of all non-header items.
        // See sample project for a visual of how these indices work.
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Change inflated layout based on 'header'.
        val layout: Int = when (viewType) {
            SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER -> R.layout.list_item_header
            SectionedRecyclerViewAdapter.VIEW_TYPE_ITEM -> R.layout.procession_view
            else -> R.layout.procession_view
        }
        val v = LayoutInflater.from(parent.context)
                .inflate(layout, parent, false)
        return ViewHolder(v)
    }

    override fun getItemViewType(section: Int, relativePosition: Int, absolutePosition: Int): Int {
        return if (section == 1) 0 else super.getItemViewType(section, relativePosition, absolutePosition) // VIEW_TYPE_HEADER is -2, VIEW_TYPE_ITEM is -1. You can return 0 or greater.
    }

    interface OpenDialogListener {
        fun onShouldOpenDialog(imageUri: String?)
        fun onNavigateToDetailsScreen(processionId: String)
        fun onNavigateToMapScreen(processionId: String)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var title = itemView.findViewById<View>(R.id.title) as TextView?
        internal var dateTitle = itemView.findViewById<View>(R.id.title_date) as TextView?
        internal var loadingTextView = itemView.findViewById<View>(R.id.loadingTextView) as TextView?
        internal var mapButton = itemView.findViewById<View>(R.id.procession_map) as Button?
        internal var detailsButton = itemView.findViewById<View>(R.id.procession_details) as Button?
        internal var processionImage = itemView.findViewById<View>(R.id.procession_image) as ImageView?
    }
}