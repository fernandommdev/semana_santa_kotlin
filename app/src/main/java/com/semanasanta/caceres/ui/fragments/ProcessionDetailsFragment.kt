package com.semanasanta.caceres.ui.fragments

import android.app.Dialog
import android.content.pm.ActivityInfo
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window

import com.github.chrisbanes.photoview.PhotoView
import com.semanasanta.caceres.MyApplication
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.model.Paso
import com.semanasanta.caceres.commons.model.Procession
import com.semanasanta.caceres.ui.adapters.CultImagesAdapter
import com.semanasanta.caceres.ui.adapters.PhotosAdapter
import com.squareup.picasso.Picasso

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList

import kotlinx.android.synthetic.main.procession_details.*


class ProcessionDetailsFragment : Fragment(), PhotosAdapter.OpenDialogListener {

    private var processionSelected: Procession? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.procession_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        val processionId = arguments?.getString("processionId")

        processionId?.let {
            val myApplication = activity?.application as MyApplication
            val processions = myApplication.processions
            processionSelected = processions.first { item -> item.uri == it }
        }

        processionSelected?.let {
            drawProcessionSelectedData(it)
        }

        processionRouteButton.setOnClickListener {
            navigateToRouteMapFragment()
        }
    }

    private fun drawProcessionSelectedData(procession: Procession) {
        drawDetailsData(procession)
        val selectedPasosList = connectedSelectedPasosList(procession)
        setPhotosViewPager(selectedPasosList)
        setRecyclerViewData(selectedPasosList)
    }

    private fun drawDetailsData(procession: Procession) {
        nameContent.text = procession.name
        commentsContent.text = procession.comments
        departureContent.text = procession.departure
        arrivalContent.text = procession.arrival
        clothesContent.text = procession.clothes
        musiciansContent.text = procession.musicians
        dateContent.text = processDate(procession.date)
    }

    private fun processDate(date: String): String {
        var processedDate = ""
        for (i in 0 until date.split("#".toRegex()).dropLastWhile { item -> item.isEmpty() }.toTypedArray().size) {
            val dtStart = date.split("#".toRegex()).dropLastWhile { item -> item.isEmpty() }.toTypedArray()[i]
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            try {
                val df = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                processedDate = processedDate + df.format(format.parse(dtStart)) + " "
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        return processedDate
    }

    private fun connectedSelectedPasosList(procession: Procession): ArrayList<Paso> {
        val myApplication = activity?.application as MyApplication
        val pasos = myApplication.pasos
        val pasosAux = ArrayList<Paso>()
        for (paso in procession.pasos) {
            pasos[paso]?.let {
                pasosAux.add(it)
            }
        }
        return pasosAux
    }

    private fun setPhotosViewPager(pasosList: ArrayList<Paso>) {
        val pagerAdapter = PhotosAdapter(activity!!, pasosList, this)
        photosViewpager.adapter = pagerAdapter
        tabLayout.setupWithViewPager(photosViewpager, true)
    }

    private fun setRecyclerViewData(pasosList: ArrayList<Paso>) {
        val adapter = CultImagesAdapter(pasosList, R.layout.pasos_items, activity!!.supportFragmentManager)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
    }

    private fun navigateToRouteMapFragment() {
        val fragment = RouteMapFragment.newInstance()
        val bundle = Bundle()
        bundle.putString("processionId", processionSelected?.uri)
        fragment.arguments = bundle
        val ft = activity?.supportFragmentManager?.beginTransaction()
        ft?.replace(R.id.fragment_container, fragment, RouteMapFragment.TAG)
        ft?.addToBackStack(RouteMapFragment.TAG)
        ft?.commit()
    }

    override fun onShouldOpenDialog(imageUri: String?) {
        val inflater = activity?.layoutInflater
        val imageDialog = inflater?.inflate(R.layout.dialog_image, null)
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(imageDialog)
        dialog.setCanceledOnTouchOutside(true)
        dialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        val imgImage = imageDialog?.findViewById<View>(R.id.full_screen_image) as PhotoView
        Picasso.with(context).load(imageUri).into(imgImage)

        dialog.show()
    }

    companion object {
        const val TAG = "ProcessionDetailsFragment"

        fun newInstance(): ProcessionDetailsFragment {
            return ProcessionDetailsFragment()
        }
    }
}
