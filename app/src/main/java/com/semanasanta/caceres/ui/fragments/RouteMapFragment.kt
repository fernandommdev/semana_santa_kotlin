package com.semanasanta.caceres.ui.fragments


import android.Manifest
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.semanasanta.caceres.MyApplication
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.network.Connection
import com.semanasanta.caceres.commons.network.Parser

import okhttp3.Response

import kotlinx.android.synthetic.main.route_maps_fragment.*


class RouteMapFragment : Fragment(), OnMapReadyCallback, Connection.RouteOperationFinishedListener, PermissionListener {

    private var mapFragment: SupportMapFragment? = null
    private var con: Connection? = null
    private var progressDialog: ProgressDialog? = null
    private var googleMap: GoogleMap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.route_maps_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        buttonReloadMap?.setOnClickListener {
            updatePoints()
        }
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        val latLng = LatLng(39.47649, -6.37224)
        val zoom = 14f
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
        checkPermission()

        updatePoints()
    }

    private fun checkPermission() {
        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(this)
                .check()
    }

    private fun updatePoints() {
        val processionId = arguments?.getString("processionId")
        processionId?.let {
            buttonReloadMap.visibility = View.GONE
            progressDialog = ProgressDialog(activity)
            progressDialog!!.setTitle(getString(R.string.retrieving_route_points))
            progressDialog!!.setMessage(getString(R.string.wait_please))
            progressDialog!!.setCancelable(false)
            progressDialog!!.show()
            con = Connection.instance
            con!!.setRouteOperationFinishedListener(this)
            try {
                con!!.requestRoute(it)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onOperationRouteSuccess(response: Response) {
        val results = Parser.parseRouteJson(response)
        val pasos = (activity!!.application as MyApplication).pasos
        activity?.runOnUiThread {
            progressDialog!!.cancel()
            var iterator = 0
            for ((_, value) in results) {
                val color = getColor(iterator++)
                val icon = getIcon(iterator)
                val polylineOptions = PolylineOptions()
                var text: String
                for (i in value.indices) {
                    if (i == 0) {
                        text = ""
                        for (j in 0 until value[i].pasos.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size) {
                            val textToAdd = value[j].pasos.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[j].replace("\\s".toRegex(), "")
                            text += pasos[textToAdd]!!.name!!
                        }
                        googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(value[i].latitude!!, value[i].longitude!!), 14f))
                        googleMap!!.addMarker(MarkerOptions()
                                .position(LatLng(value[i].latitude!!, value[i].longitude!!))
                                .title(getString(R.string.start))
                                .icon(BitmapDescriptorFactory.defaultMarker(icon))
                                .snippet(if (value[i].pasos.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) "Conjunta" else text))
                    } else if (i == value.size - 1) {
                        text = ""
                        for (j in 0 until value[i].pasos.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size) {
                            val textToAdd = value[j].pasos.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[j].replace("\\s".toRegex(), "")
                            text += pasos[textToAdd]!!.name!!
                        }
                        googleMap!!.addMarker(MarkerOptions()
                                .position(LatLng(value[i].latitude!!, value[i].longitude!!))
                                .title(getString(R.string.end))
                                .icon(BitmapDescriptorFactory.defaultMarker(icon))
                                .snippet(if (value[i].pasos.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) "Conjunta" else text))
                    }
                    googleMap!!.addPolyline(polylineOptions.add(LatLng(value[i].latitude!!, value[i].longitude!!),
                            LatLng(value[i].latitude!!, value[i].longitude!!)).width(4f)
                            .color(color))
                }
            }
        }
    }

    override fun onOperationFailed() {
        activity?.runOnUiThread {
            buttonReloadMap.visibility = View.VISIBLE
            progressDialog!!.cancel()
            Toast.makeText(activity, getString(R.string.error_downloading_data), Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("MissingPermission")
    override fun onPermissionGranted(response: PermissionGrantedResponse) {
        googleMap?.isMyLocationEnabled = true
    }

    override fun onPermissionDenied(response: PermissionDeniedResponse) {

    }

    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
        token.continuePermissionRequest()
    }

    private fun getColor(position: Int): Int {
        return when (position) {
            0 -> Color.parseColor("#1E90FF")
            1 -> Color.parseColor("#48D1CC")
            2 -> Color.RED
            else -> Color.MAGENTA
        }
    }

    private fun getIcon(position: Int): Float {
        return when (position) {
            1 -> BitmapDescriptorFactory.HUE_AZURE
            2 -> BitmapDescriptorFactory.HUE_CYAN
            3 -> BitmapDescriptorFactory.HUE_RED
            else -> BitmapDescriptorFactory.HUE_RED
        }
    }

    companion object {
        const val TAG = "RouteMapFragment"

        fun newInstance(): RouteMapFragment {
            return RouteMapFragment()
        }
    }
}
