package com.semanasanta.caceres.ui.adapters

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.model.Brotherhood
import com.semanasanta.caceres.ui.fragments.BrotherhoodDetailsFragment
import java.util.*

class BrotherhoodsListAdapter(private val items: ArrayList<Brotherhood>, private val itemLayout: Int, private val fm: FragmentManager) : RecyclerView.Adapter<BrotherhoodsListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(itemLayout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = items[position].popularName
        holder.subTitle.text = items[position].name
        holder.layoutBrotherhoodItem.setOnClickListener {
            val fragment = BrotherhoodDetailsFragment.newInstance()
            val bundle = Bundle()
            bundle.putString("brotherhoodId", items[position].uri)
            fragment.arguments = bundle
            val ft = fm.beginTransaction()
            ft.replace(R.id.fragment_container, fragment, BrotherhoodDetailsFragment.TAG)
            ft.addToBackStack(null)
            ft.commit()
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var layoutBrotherhoodItem: RelativeLayout = itemView.findViewById<View>(R.id.layout_brotherhood_item) as RelativeLayout
        var title: TextView = itemView.findViewById<View>(R.id.brotherhood_title) as TextView
        var subTitle: TextView = itemView.findViewById<View>(R.id.brotherhood_subtitle) as TextView
    }
}
