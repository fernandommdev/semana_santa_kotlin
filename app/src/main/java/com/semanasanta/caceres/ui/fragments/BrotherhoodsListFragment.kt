package com.semanasanta.caceres.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.semanasanta.caceres.MyApplication
import com.semanasanta.caceres.R
import com.semanasanta.caceres.ui.adapters.BrotherhoodsListAdapter

import kotlinx.android.synthetic.main.fragment_brotherhoods_list.*

class BrotherhoodsListFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
       return inflater.inflate(R.layout.fragment_brotherhoods_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val myApplication = activity?.application as MyApplication
        val brotherhoods = myApplication.brotherhoods
        val adapter = BrotherhoodsListAdapter(brotherhoods, R.layout.brotherhood_list_item, fragmentManager!!)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    companion object {
        const val TAG = "BrotherhoodsListFragment"

        fun newInstance(): BrotherhoodsListFragment {
            return BrotherhoodsListFragment()
        }
    }
}
