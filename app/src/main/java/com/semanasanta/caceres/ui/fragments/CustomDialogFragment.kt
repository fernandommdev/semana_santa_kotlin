package com.semanasanta.caceres.ui.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Window

import com.semanasanta.caceres.R

class CustomDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = activity?.layoutInflater

        val view = inflater?.inflate(R.layout.dialog_info_layout, null)

        val dialog = AlertDialog.Builder(activity).setView(view)
                .setPositiveButton(getString(R.string.rate_app)
                ) { dialog, _ ->
                    val browse = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.semanasanta.caceres"))

                    startActivity(browse)
                    dialog.dismiss()
                }
                .setNegativeButton(getString(R.string.close)
                ) { dialog, _ ->
                    dialog.dismiss()
                }.create()
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    companion object {
        fun newInstance(): CustomDialogFragment {
            return CustomDialogFragment()
        }
    }

}