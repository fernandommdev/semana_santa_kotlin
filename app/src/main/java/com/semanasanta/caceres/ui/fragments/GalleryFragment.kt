package com.semanasanta.caceres.ui.fragments

import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window

import com.github.chrisbanes.photoview.PhotoView
import com.semanasanta.caceres.MyApplication
import com.semanasanta.caceres.R
import com.semanasanta.caceres.ui.adapters.GalleryAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.gallery_fragment.*

import java.util.ArrayList

class GalleryFragment : Fragment(), GalleryAdapter.OpenDialogListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.gallery_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val myApplication = activity?.application as MyApplication
        val pasosAux = ArrayList(myApplication.pasos.values)

        val galleryAdapter = GalleryAdapter(pasosAux, R.layout.gallery_card, activity!!, this)
        recyclerView.layoutManager = GridLayoutManager(context, 2)
        recyclerView.adapter = galleryAdapter
    }

    override fun onShouldOpenDialog(imageUri: String) {
        val inflater = activity?.layoutInflater
        val imageDialog = inflater?.inflate(R.layout.dialog_image, null)
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(imageDialog)
        dialog.setCanceledOnTouchOutside(true)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        val imgImage = imageDialog?.findViewById<PhotoView>(R.id.full_screen_image)
        Picasso.with(context).load(imageUri).into(imgImage)

        dialog.show()
    }

    companion object {
        const val TAG = "GalleryFragment"

        fun newInstance(): GalleryFragment {
            return GalleryFragment()
        }
    }

}
