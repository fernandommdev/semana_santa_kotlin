package com.semanasanta.caceres.ui.fragments

import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window

import com.github.chrisbanes.photoview.PhotoView
import com.semanasanta.caceres.MyApplication
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.model.Festivity
import com.semanasanta.caceres.ui.adapters.ProcessionsAdapter
import com.squareup.picasso.Picasso

import java.util.ArrayList
import java.util.Arrays

import kotlinx.android.synthetic.main.processions_fragment.*


class ProcessionsListFragment : Fragment(), ProcessionsAdapter.OpenDialogListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.processions_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val myApplication = activity?.application as MyApplication
        val result = myApplication.processions
        val pasos = myApplication.pasos

        val festivity1 = Festivity(getString(R.string.passion_saturday), "24/03/2018", ArrayList())
        val festivity2 = Festivity(getString(R.string.palm_sunday), "25/03/2018", ArrayList())
        val festivity3 = Festivity(getString(R.string.holy_monday), "26/03/2018", ArrayList())
        val festivity4 = Festivity(getString(R.string.holy_tuesday), "27/03/2018", ArrayList())
        val festivity5 = Festivity(getString(R.string.holy_wednesday), "28/03/2018", ArrayList())
        val festivity6 = Festivity(getString(R.string.holy_thursday), "29/03/2018", ArrayList())
        val festivity7 = Festivity(getString(R.string.holy_friday), "30/03/2018", ArrayList())
        val festivity8 = Festivity(getString(R.string.glory_saturday), "31/03/2018", ArrayList())
        val festivity9 = Festivity(getString(R.string.resurrection_sunday), "01/04/2018", ArrayList())

        for (procession in result) {
            when {
                procession.date.contains("2018-03-24") -> festivity1.addProcession(procession)
                procession.date.contains("2018-03-25") -> festivity2.addProcession(procession)
                procession.date.contains("2018-03-26") -> festivity3.addProcession(procession)
                procession.date.contains("2018-03-27") -> festivity4.addProcession(procession)
                procession.date.contains("2018-03-28") -> festivity5.addProcession(procession)
                procession.date.contains("2018-03-29") -> festivity6.addProcession(procession)
                procession.date.contains("2018-03-30") -> festivity7.addProcession(procession)
                procession.date.contains("2018-03-31") -> festivity8.addProcession(procession)
                procession.date.contains("2018-04-01") -> festivity9.addProcession(procession)
            }
        }

        val festivities = Arrays.asList(festivity1, festivity2, festivity3, festivity4,
                festivity5, festivity6, festivity7, festivity8, festivity9)

        val adapter = ProcessionsAdapter(festivities, pasos, activity!!, this)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    override fun onShouldOpenDialog(imageUri: String?) {
        activity?.let {
            val inflater = it.layoutInflater
            val imageDialog = inflater.inflate(R.layout.dialog_image, null)
            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(imageDialog)
            dialog.setCanceledOnTouchOutside(true)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

            val imgImage = imageDialog?.findViewById<PhotoView>(R.id.full_screen_image)
            Picasso.with(context).load(imageUri).into(imgImage)

            dialog.show()
        }
    }

    override fun onNavigateToDetailsScreen(processionId: String) {
        val fragment = ProcessionDetailsFragment.newInstance()
        val bundle = Bundle()
        bundle.putString("processionId", processionId)
        //bundle.putInt("pasoToShow", finalPasoPositionSelected)
        fragment.arguments = bundle
        val ft = fragmentManager?.beginTransaction()
        ft?.replace(R.id.fragment_container, fragment, ProcessionDetailsFragment.TAG)
        ft?.addToBackStack(ProcessionDetailsFragment.TAG)
        ft?.commit()
    }

    override fun onNavigateToMapScreen(processionId: String) {
        val fragment = RouteMapFragment.newInstance()
        val bundle = Bundle()
        bundle.putString("processionId", processionId)
        fragment.arguments = bundle
        val ft = fragmentManager?.beginTransaction()
        ft?.replace(R.id.fragment_container, fragment, RouteMapFragment.TAG)
        ft?.addToBackStack(RouteMapFragment.TAG)
        ft?.commit()
    }

    companion object {
        const val TAG = "ProcessionsListFragment"

        fun newInstance(): ProcessionsListFragment {
            return ProcessionsListFragment()
        }
    }
}
