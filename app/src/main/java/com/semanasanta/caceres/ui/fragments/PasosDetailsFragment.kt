package com.semanasanta.caceres.ui.fragments

import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.github.chrisbanes.photoview.PhotoView
import com.semanasanta.caceres.MyApplication
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.model.Paso
import com.semanasanta.caceres.commons.tools.ImageLoader
import com.semanasanta.caceres.ui.adapters.PhotosAdapterPaso
import kotlinx.android.synthetic.main.pasos_details.*


class PasosDetailsFragment : Fragment(), PhotosAdapterPaso.OpenDialogListener, View.OnClickListener {

    private var pasoSelected: Paso? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.pasos_details, container, false)

        val pasoId = arguments?.getString("pasoId")
        if (pasoId != null) {
            val myApplication = activity?.application as MyApplication
            val pasos = myApplication.pasos
            pasoSelected = pasos[pasoId]
        }

        val pager = view.findViewById<ViewPager>(R.id.photos_viewpager)
        val pagerAdapter = PhotosAdapterPaso(context!!, pasoSelected!!, this)
        pager.adapter = pagerAdapter
        val tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
        tabLayout.setupWithViewPager(pager, true)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drawDetailsData()
        floatingButton.setOnClickListener(this)
    }

    private fun drawDetailsData() {
        pasoSelected?.let {
            nameContent.text = it.name
            commentsContent.text = it.comments
            orderContent.text = it.order
            brothersContent.text = it.brothers
            typeContent.text = it.type
        }
    }

    override fun onClick(v: View?) {
        if(v?.id == floatingButton.id) {
            val fragment = RouteMapFragment.newInstance()
            val bundle = Bundle()
            bundle.putString("processionId", pasoSelected!!.processionUri)
            fragment.arguments = bundle
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.fragment_container, fragment, RouteMapFragment.TAG)
            ft?.addToBackStack(RouteMapFragment.TAG)
            ft?.commit()
        }
    }

    override fun onShouldOpenDialog(imageUri: String) {
        val inflater = activity?.layoutInflater
        val imageDialog = inflater?.inflate(R.layout.dialog_image, null)
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(imageDialog)
        dialog.setCanceledOnTouchOutside(true)
        dialog.window.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        val imgImage = imageDialog?.findViewById<View>(R.id.full_screen_image) as PhotoView
        ImageLoader.loadSimpleImageOnImageView(context!!, imageUri, imgImage)

        dialog.show()
    }

    companion object {

        const val TAG = "PasosDetailsFragment"

        fun newInstance(): PasosDetailsFragment {
            return PasosDetailsFragment()
        }
    }
}
