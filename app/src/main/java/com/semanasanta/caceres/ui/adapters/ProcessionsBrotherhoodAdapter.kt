package com.semanasanta.caceres.ui.adapters

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.model.Procession
import com.semanasanta.caceres.ui.fragments.ProcessionDetailsFragment
import java.util.*

class ProcessionsBrotherhoodAdapter(private val items: ArrayList<Procession>, private val itemLayout: Int, private val fm: FragmentManager) : RecyclerView.Adapter<ProcessionsBrotherhoodAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(itemLayout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.text.text = items[position].name
        holder.buttonDetails.setOnClickListener {
            val fragment = ProcessionDetailsFragment.newInstance()
            val bundle = Bundle()
            bundle.putString("processionId", items[position].uri)
            fragment.arguments = bundle
            val ft = fm.beginTransaction()
            ft.replace(R.id.fragment_container, fragment, ProcessionDetailsFragment.TAG)
            ft.addToBackStack(null)
            ft.commit()
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var text: TextView = itemView.findViewById<View>(R.id.pasos_textview) as TextView
        val buttonDetails: Button = itemView.findViewById<View>(R.id.pasos_button) as Button
    }
}
