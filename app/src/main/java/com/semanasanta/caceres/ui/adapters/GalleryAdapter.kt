package com.semanasanta.caceres.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.model.Paso
import com.semanasanta.caceres.commons.tools.ImageLoader
import com.squareup.picasso.Callback
import java.util.*

class GalleryAdapter(private val items: ArrayList<Paso>, private val itemLayout: Int, private val context: Context, private val op: OpenDialogListener) : RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(itemLayout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = items[position].name
        holder.subTitle.text = "Autor: " + items[position].imageAuthor
        ImageLoader.loadImageOnImageView(context, items[position].imageUri, holder.image, object : Callback {
            override fun onSuccess() {
                holder.loadingTextView.visibility = View.GONE
            }

            override fun onError() {
                holder.loadingTextView.text = context.getString(R.string.error_loading_images)
            }
        })
        holder.image.setOnClickListener { op.onShouldOpenDialog(items[position].imageUri) }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    interface OpenDialogListener {
        fun onShouldOpenDialog(imageUri: String)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById<View>(R.id.title) as TextView
        var subTitle: TextView = itemView.findViewById<View>(R.id.author_name) as TextView
        var image: ImageView = itemView.findViewById<View>(R.id.thumbnail) as ImageView
        var loadingTextView: TextView = itemView.findViewById<View>(R.id.loadingTextView) as TextView

    }
}
