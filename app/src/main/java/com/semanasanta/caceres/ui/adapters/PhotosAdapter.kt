package com.semanasanta.caceres.ui.adapters

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.model.Paso
import com.semanasanta.caceres.commons.tools.ImageLoader
import com.squareup.picasso.Callback
import java.util.*

class PhotosAdapter(private val context: Context, private val pasos: ArrayList<Paso>, private val op: OpenDialogListener) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.viewpager_element, container, false) as ViewGroup
        val displayImage = view.findViewById<View>(R.id.imageViewPager) as ImageView
        val loadingTextView = view.findViewById<View>(R.id.loadingTextView) as TextView

        ImageLoader.loadImageOnImageView(context, pasos[position].imageUri, displayImage, object : Callback {
            override fun onSuccess() {
                loadingTextView.visibility = View.GONE
            }

            override fun onError() {
                loadingTextView.text = context.getString(R.string.error_loading_images)
            }
        })

        displayImage.setOnClickListener { op.onShouldOpenDialog(pasos[position].imageUri) }
        container.addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return pasos.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }

    interface OpenDialogListener {
        fun onShouldOpenDialog(imageUri: String?)
    }
}
