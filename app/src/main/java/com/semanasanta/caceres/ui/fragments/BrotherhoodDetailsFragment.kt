package com.semanasanta.caceres.ui.fragments

import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.github.chrisbanes.photoview.PhotoView
import com.semanasanta.caceres.MyApplication
import com.semanasanta.caceres.R
import com.semanasanta.caceres.commons.model.Brotherhood
import com.semanasanta.caceres.commons.model.Procession
import com.semanasanta.caceres.commons.model.RssEntry
import com.semanasanta.caceres.ui.adapters.PhotosAdapterPaso
import com.semanasanta.caceres.ui.adapters.ProcessionsBrotherhoodAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.brotherhood_details.*
import java.util.*

class BrotherhoodDetailsFragment : Fragment(), PhotosAdapterPaso.OpenDialogListener, View.OnClickListener {

    // ///////////////////////////////////////////////////////////////////////////
    // Logic
    // ///////////////////////////////////////////////////////////////////////////

    private var brotherhoodSelected: Brotherhood? = null

    private var rssEntryHashMap: HashMap<RssEntry.SocialNetwork, RssEntry>? = HashMap()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val brotherhoodId = arguments?.getString("brotherhoodId")
        brotherhoodId?.let {
            val myApplication = activity?.application as MyApplication
            val brotherhoods = myApplication.brotherhoods
            brotherhoodSelected = brotherhoods.first { item -> item.uri == it }
        }

        rssEntryHashMap = brotherhoodSelected?.rssEntries

        return inflater.inflate(R.layout.brotherhood_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drawBrotherHoodData()
        setButtonsListeners()
    }

    private fun drawBrotherHoodData() {
        nameContent.text = brotherhoodSelected?.popularName
        fullNameContent.text = brotherhoodSelected?.name
        if (brotherhoodSelected?.web?.isEmpty() == true) {
            webLayout.visibility = View.GONE
        } else {
            webContent.text = brotherhoodSelected?.web
            Linkify.addLinks(webContent, Linkify.WEB_URLS)
        }
        brothersContent.text = brotherhoodSelected?.members
        foundedContent.text = brotherhoodSelected?.founded

        fillAdapterWithProcessions()
        showRssIcons()
    }

    private fun fillAdapterWithProcessions() {
        val myApplication = activity?.application as MyApplication
        val processions = myApplication.processions
        val processionsAux = ArrayList<Procession>()
        for (procession in processions) {
            val uriProcession = brotherhoodSelected?.uriProcession
            uriProcession?.let {
                if(it.any { item -> item == procession.uri}) {
                    processionsAux.add(procession)
                }
            }
        }
        val adapter = ProcessionsBrotherhoodAdapter(processionsAux, R.layout.pasos_items, activity!!.supportFragmentManager)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
    }

    private fun showRssIcons() {
        if (rssEntryHashMap?.isEmpty() == true) {
            rssLinksLayout.visibility = View.GONE
        } else {
            if (rssEntryHashMap?.get(RssEntry.SocialNetwork.LINKEDIN) != null) {
                linkedInImageView.visibility = View.VISIBLE
            }
            if (rssEntryHashMap?.get(RssEntry.SocialNetwork.FACEBOOK) != null) {
                facebookImageView.visibility = View.VISIBLE
            }
            if (rssEntryHashMap?.get(RssEntry.SocialNetwork.PINTEREST) != null) {
                pinterestImageView.visibility = View.VISIBLE
            }
            if (rssEntryHashMap?.get(RssEntry.SocialNetwork.INSTAGRAM) != null) {
                instagramImageView.visibility = View.VISIBLE
            }
            if (rssEntryHashMap?.get(RssEntry.SocialNetwork.TWITTER) != null) {
                twitterImageView.visibility = View.VISIBLE
            }
            if (rssEntryHashMap?.get(RssEntry.SocialNetwork.WIKIPEDIA) != null) {
                wikipediaImageView.visibility = View.VISIBLE
            }
        }
    }

    private fun setButtonsListeners() {
        linkedInImageView.setOnClickListener(this)
        facebookImageView.setOnClickListener(this)
        twitterImageView.setOnClickListener(this)
        pinterestImageView.setOnClickListener(this)
        instagramImageView.setOnClickListener(this)
        wikipediaImageView.setOnClickListener(this)
    }

    // ///////////////////////////////////////////////////////////////////////////
    // Onclick Events
    // ///////////////////////////////////////////////////////////////////////////

    override fun onClick(v: View?) {
        when(view?.id) {
            R.id.linkedInImageView -> {
                launchUrl(rssEntryHashMap?.get(RssEntry.SocialNetwork.LINKEDIN)?.url)
            }
            R.id.facebookImageView -> {
                launchUrl(rssEntryHashMap?.get(RssEntry.SocialNetwork.FACEBOOK)?.url)
            }
            R.id.twitterImageView -> {
                launchUrl(rssEntryHashMap?.get(RssEntry.SocialNetwork.TWITTER)?.url)
            }
            R.id.pinterestImageView -> {
                launchUrl(rssEntryHashMap?.get(RssEntry.SocialNetwork.PINTEREST)?.url)
            }
            R.id.wikipediaImageView -> {
                launchUrl(rssEntryHashMap?.get(RssEntry.SocialNetwork.WIKIPEDIA)?.url)
            }
        }
    }

    // ///////////////////////////////////////////////////////////////////////////
    // Methods
    // ///////////////////////////////////////////////////////////////////////////

    private fun launchUrl(url: String?) {
        url?.let {
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            intent.addCategory(Intent.CATEGORY_BROWSABLE)
            intent.data = Uri.parse(url)
            startActivity(intent)
        }
    }


    override fun onShouldOpenDialog(imageUri: String) {
        val inflater = activity?.layoutInflater
        val imageDialog = inflater?.inflate(R.layout.dialog_image, null)
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(imageDialog)
        dialog.setCanceledOnTouchOutside(true)
        dialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        val imgImage = imageDialog?.findViewById<View>(R.id.full_screen_image) as PhotoView
        Picasso.with(context).load(imageUri).into(imgImage)

        dialog.show()
    }

    companion object {

        const val TAG = "BrotherhoodDetailsFragment"

        fun newInstance(): BrotherhoodDetailsFragment {
            return BrotherhoodDetailsFragment()
        }
    }
}
